#!/usr/bin/env python

"""
 Return a PNG graph from rrd files...
"""

import cgitb
#cgitb.enable()
cgitb.enable(display=0, logdir="/tmp")

import re
import os
import cgi
import sys
from string import Template

import socket

graph_header = """
rrdtool graph -                   
    -a PNG -w $WIDTH -h $HEIGHT  
    --start='$START' --end='$END'
"""

THEME_TAGS = ['BACK', 'SHADEA', 'SHADEB', 'FONT',
    'CANVAS', 'GRID', 'MGRID', 'FRAME', 'ARROW']

graph_templates = {
    "memory": """
        --title="Speicherauslastung"
        -l 0
        "DEF:buffered=$COLLECTDRRD/$HOST/memory/memory-buffered.rrd:value:AVERAGE"
        "DEF:cached=$COLLECTDRRD/$HOST/memory/memory-cached.rrd:value:AVERAGE"
        "DEF:free=$COLLECTDRRD/$HOST/memory/memory-free.rrd:value:AVERAGE"
        "DEF:used=$COLLECTDRRD/$HOST/memory/memory-used.rrd:value:AVERAGE"
        "AREA:used#$ACOLOR1::"
        "AREA:buffered#$ACOLOR2::STACK"
        "AREA:cached#$ACOLOR4::STACK"
        "AREA:free#$ACOLOR3::STACK"
        "LINE1:used#$LCOLOR1:used:"
        "LINE1:buffered#$LCOLOR2:buffered:STACK"
        "LINE1:cached#$LCOLOR4:cached:STACK"
        "LINE1:free#$LCOLOR3:free:STACK"
    """,
    "disk": """
        --title="Disk IO in Bytes/s"
        DEF:sda_read=$COLLECTDRRD/$HOST/disk-vda/disk_octets.rrd:read:AVERAGE
        DEF:sdb_read=$COLLECTDRRD/$HOST/disk-vdb/disk_octets.rrd:read:AVERAGE
        DEF:sda_write=$COLLECTDRRD/$HOST/disk-vda/disk_octets.rrd:write:AVERAGE
        DEF:sdb_write=$COLLECTDRRD/$HOST/disk-vdb/disk_octets.rrd:write:AVERAGE
        CDEF:sda_write_m=-1,sda_write,*
        CDEF:sdb_write_m=-1,sdb_write,*
        AREA:sda_read#$ACOLOR4::                     
        AREA:sdb_read#$ACOLOR4::STACK
        LINE1:sda_read#$LCOLOR4::
        LINE1:sdb_read#$LCOLOR4::STACK
        AREA:sda_write_m#$ACOLOR1::                     
        AREA:sdb_write_m#$ACOLOR1::STACK
        LINE1:sda_write_m#$LCOLOR1::
        LINE1:sdb_write_m#$LCOLOR1::STACK
    """, 
    "disk-ops": """
        --title="Disk IO in Ops/s"
        DEF:sda_read=$COLLECTDRRD/$HOST/disk-sda/disk_ops.rrd:read:AVERAGE
        DEF:sdb_read=$COLLECTDRRD/$HOST/disk-sdb/disk_ops.rrd:read:AVERAGE
        DEF:sdc_read=$COLLECTDRRD/$HOST/disk-sdc/disk_ops.rrd:read:AVERAGE
        DEF:sdd_read=$COLLECTDRRD/$HOST/disk-sdd/disk_ops.rrd:read:AVERAGE
        DEF:sda_write=$COLLECTDRRD/$HOST/disk-sda/disk_ops.rrd:write:AVERAGE
        DEF:sdb_write=$COLLECTDRRD/$HOST/disk-sdb/disk_ops.rrd:write:AVERAGE
        DEF:sdc_write=$COLLECTDRRD/$HOST/disk-sdc/disk_ops.rrd:write:AVERAGE
        DEF:sdd_write=$COLLECTDRRD/$HOST/disk-sdd/disk_ops.rrd:write:AVERAGE
        CDEF:sda_write_m=-1,sda_write,*
        CDEF:sdb_write_m=-1,sdb_write,*
        CDEF:sdc_write_m=-1,sdc_write,*
        CDEF:sdd_write_m=-1,sdd_write,*
        AREA:sda_read#$ACOLOR4::                     
        AREA:sdb_read#$ACOLOR4::STACK
        AREA:sdc_read#$ACOLOR4::STACK
        AREA:sdd_read#$ACOLOR4::STACK
        LINE1:sda_read#$LCOLOR4::
        LINE1:sdb_read#$LCOLOR4::STACK
        LINE1:sdc_read#$LCOLOR4::STACK
        LINE1:sdd_read#$LCOLOR4::STACK
        AREA:sda_write_m#$ACOLOR1::                     
        AREA:sdb_write_m#$ACOLOR1::STACK
        AREA:sdc_write_m#$ACOLOR1::STACK
        AREA:sdd_write_m#$ACOLOR1::STACK
        LINE1:sda_write_m#$LCOLOR1::
        LINE1:sdb_write_m#$LCOLOR1::STACK
        LINE1:sdc_write_m#$LCOLOR1::STACK
        LINE1:sdd_write_m#$LCOLOR1::STACK
    """, 
    "load": """
        --title="System Load"
        -l 0
        -u 1
        DEF:load=$COLLECTDRRD/$HOST/load/load.rrd:shortterm:AVERAGE
        DEF:load_=$COLLECTDRRD/$HOST/load/load.rrd:shortterm:MAX
        AREA:load_#$ACOLOR1::                     
        LINE1:load#$LCOLOR1::               
    """, 
    "cpu1": """
        --title="CPU Nutzung"
        -l 0
        -u 100
        DEF:cpu-idle=$COLLECTDRRD/$HOST/cpu-0/cpu-idle.rrd:value:AVERAGE
        DEF:cpu-interrupt=$COLLECTDRRD/$HOST/cpu-0/cpu-interrupt.rrd:value:AVERAGE
        DEF:cpu-nice=$COLLECTDRRD/$HOST/cpu-0/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-softirq=$COLLECTDRRD/$HOST/cpu-0/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-steal=$COLLECTDRRD/$HOST/cpu-0/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-system=$COLLECTDRRD/$HOST/cpu-0/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-user=$COLLECTDRRD/$HOST/cpu-0/cpu-user.rrd:value:AVERAGE           
        AREA:cpu-steal#$ACOLOR1::                     
        AREA:cpu-system#$ACOLOR1::STACK               
        AREA:cpu-interrupt#$ACOLOR2::STACK            
        AREA:cpu-softirq#$ACOLOR2::STACK              
        AREA:cpu-user#$ACOLOR4::STACK                 
        AREA:cpu-nice#$ACOLOR3::STACK                 
        LINE1:cpu-steal#$LCOLOR1::               
        LINE1:cpu-system#$LCOLOR1:system:STACK        
        LINE1:cpu-interrupt#$LCOLOR2::STACK  
        LINE1:cpu-softirq#$LCOLOR2:irq:STACK     
        LINE1:cpu-user#$LCOLOR4:user:STACK            
        LINE1:cpu-nice#$LCOLOR3:nice:STACK
    """, 
    "cpu2": """
        --title="CPU Nutzung"
        -l 0
        -u 200
        DEF:cpu-0-idle=$COLLECTDRRD/$HOST/cpu-0/cpu-idle.rrd:value:AVERAGE
        DEF:cpu-0-interrupt=$COLLECTDRRD/$HOST/cpu-0/cpu-interrupt.rrd:value:AVERAGE
        DEF:cpu-0-nice=$COLLECTDRRD/$HOST/cpu-0/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-0-softirq=$COLLECTDRRD/$HOST/cpu-0/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-0-steal=$COLLECTDRRD/$HOST/cpu-0/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-0-system=$COLLECTDRRD/$HOST/cpu-0/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-0-user=$COLLECTDRRD/$HOST/cpu-0/cpu-user.rrd:value:AVERAGE           
        DEF:cpu-1-idle=$COLLECTDRRD/$HOST/cpu-1/cpu-idle.rrd:value:AVERAGE           
        DEF:cpu-1-interrupt=$COLLECTDRRD/$HOST/cpu-1/cpu-interrupt.rrd:value:AVERAGE 
        DEF:cpu-1-nice=$COLLECTDRRD/$HOST/cpu-1/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-1-softirq=$COLLECTDRRD/$HOST/cpu-1/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-1-steal=$COLLECTDRRD/$HOST/cpu-1/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-1-system=$COLLECTDRRD/$HOST/cpu-1/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-1-user=$COLLECTDRRD/$HOST/cpu-1/cpu-user.rrd:value:AVERAGE           
        CDEF:cpu-idle=cpu-0-idle,cpu-1-idle,+
        CDEF:cpu-interrupt=cpu-0-interrupt,cpu-1-interrupt,+
        CDEF:cpu-nice=cpu-0-nice,cpu-1-nice,+
        CDEF:cpu-softirq=cpu-0-softirq,cpu-1-softirq,+
        CDEF:cpu-steal=cpu-0-steal,cpu-1-steal,+
        CDEF:cpu-system=cpu-0-system,cpu-1-system,+
        CDEF:cpu-user=cpu-0-user,cpu-1-user,+
        AREA:cpu-steal#$ACOLOR1::                     
        AREA:cpu-system#$ACOLOR1::STACK               
        AREA:cpu-interrupt#$ACOLOR2::STACK            
        AREA:cpu-softirq#$ACOLOR2::STACK              
        AREA:cpu-user#$ACOLOR4::STACK                 
        AREA:cpu-nice#$ACOLOR3::STACK                 
        LINE1:cpu-steal#94541FC0:steal:               
        LINE1:cpu-system#94541FC0:system:STACK        
        LINE1:cpu-interrupt#94541FC0:interrupt:STACK  
        LINE1:cpu-softirq#94541FC0:soft-irq:STACK     
        LINE1:cpu-user#948E1FC0:user:STACK            
        LINE1:cpu-nice#1F9454C0:nice:STACK
    """, 
    "cpu4": """
        --title="CPU Nutzung"
        -l 0
        -u 400
        DEF:cpu-0-idle=$COLLECTDRRD/$HOST/cpu-0/cpu-idle.rrd:value:AVERAGE
        DEF:cpu-0-interrupt=$COLLECTDRRD/$HOST/cpu-0/cpu-interrupt.rrd:value:AVERAGE
        DEF:cpu-0-nice=$COLLECTDRRD/$HOST/cpu-0/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-0-softirq=$COLLECTDRRD/$HOST/cpu-0/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-0-steal=$COLLECTDRRD/$HOST/cpu-0/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-0-system=$COLLECTDRRD/$HOST/cpu-0/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-0-user=$COLLECTDRRD/$HOST/cpu-0/cpu-user.rrd:value:AVERAGE           
        DEF:cpu-1-idle=$COLLECTDRRD/$HOST/cpu-1/cpu-idle.rrd:value:AVERAGE           
        DEF:cpu-1-interrupt=$COLLECTDRRD/$HOST/cpu-1/cpu-interrupt.rrd:value:AVERAGE 
        DEF:cpu-1-nice=$COLLECTDRRD/$HOST/cpu-1/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-1-softirq=$COLLECTDRRD/$HOST/cpu-1/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-1-steal=$COLLECTDRRD/$HOST/cpu-1/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-1-system=$COLLECTDRRD/$HOST/cpu-1/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-1-user=$COLLECTDRRD/$HOST/cpu-1/cpu-user.rrd:value:AVERAGE           
        DEF:cpu-2-idle=$COLLECTDRRD/$HOST/cpu-2/cpu-idle.rrd:value:AVERAGE           
        DEF:cpu-2-interrupt=$COLLECTDRRD/$HOST/cpu-2/cpu-interrupt.rrd:value:AVERAGE 
        DEF:cpu-2-nice=$COLLECTDRRD/$HOST/cpu-2/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-2-softirq=$COLLECTDRRD/$HOST/cpu-2/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-2-steal=$COLLECTDRRD/$HOST/cpu-2/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-2-system=$COLLECTDRRD/$HOST/cpu-2/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-2-user=$COLLECTDRRD/$HOST/cpu-2/cpu-user.rrd:value:AVERAGE           
        DEF:cpu-3-idle=$COLLECTDRRD/$HOST/cpu-2/cpu-idle.rrd:value:AVERAGE           
        DEF:cpu-3-interrupt=$COLLECTDRRD/$HOST/cpu-3/cpu-interrupt.rrd:value:AVERAGE 
        DEF:cpu-3-nice=$COLLECTDRRD/$HOST/cpu-3/cpu-nice.rrd:value:AVERAGE           
        DEF:cpu-3-softirq=$COLLECTDRRD/$HOST/cpu-3/cpu-softirq.rrd:value:AVERAGE     
        DEF:cpu-3-steal=$COLLECTDRRD/$HOST/cpu-3/cpu-steal.rrd:value:AVERAGE         
        DEF:cpu-3-system=$COLLECTDRRD/$HOST/cpu-3/cpu-system.rrd:value:AVERAGE       
        DEF:cpu-3-user=$COLLECTDRRD/$HOST/cpu-3/cpu-user.rrd:value:AVERAGE           
        CDEF:cpu-idle=cpu-0-idle,cpu-1-idle,cpu-2-idle,cpu-3-idle,+,+,+
        CDEF:cpu-interrupt=cpu-0-interrupt,cpu-1-interrupt,cpu-2-interrupt,cpu-3-interrupt,+,+,+
        CDEF:cpu-nice=cpu-0-nice,cpu-1-nice,cpu-2-nice,cpu-3-nice,+,+,+
        CDEF:cpu-softirq=cpu-0-softirq,cpu-1-softirq,cpu-2-softirq,cpu-3-softirq,+,+,+
        CDEF:cpu-steal=cpu-0-steal,cpu-1-steal,cpu-2-steal,cpu-3-steal,+,+,+
        CDEF:cpu-system=cpu-0-system,cpu-1-system,cpu-2-system,cpu-3-system,+,+,+
        CDEF:cpu-user=cpu-0-user,cpu-1-user,cpu-2-user,cpu-3-user,+,+,+
        AREA:cpu-steal#$ACOLOR1::                     
        AREA:cpu-system#$ACOLOR1::STACK               
        AREA:cpu-interrupt#$ACOLOR2::STACK            
        AREA:cpu-softirq#$ACOLOR2::STACK              
        AREA:cpu-user#$ACOLOR4::STACK                 
        AREA:cpu-nice#$ACOLOR3::STACK                 
        LINE1:cpu-steal#94541FC0:steal:               
        LINE1:cpu-system#94541FC0:system:STACK        
        LINE1:cpu-interrupt#94541FC0:interrupt:STACK  
        LINE1:cpu-softirq#94541FC0:soft-irq:STACK     
        LINE1:cpu-user#948E1FC0:user:STACK            
        LINE1:cpu-nice#1F9454C0:nice:STACK
    """, 
    "eth0": """
        --title="Uplink [Bytes/s]"
        -E
        'DEF:rx=$COLLECTDRRD/$HOST/interface/if_octets-eth0.rrd:rx:AVERAGE'
        'DEF:rx_=$COLLECTDRRD/$HOST/interface/if_octets-eth0.rrd:rx:MAX'
        'DEF:tx=$COLLECTDRRD/$HOST/interface/if_octets-eth0.rrd:tx:AVERAGE'
        'DEF:tx_=$COLLECTDRRD/$HOST/interface/if_octets-eth0.rrd:tx:MAX'
        'DEF:rx6=$COLLECTDRRD/$HOST/interface/if_octets-tun6to4.rrd:rx:AVERAGE'
        'DEF:rx6_=$COLLECTDRRD/$HOST/interface/if_octets-tun6to4.rrd:rx:MAX'
        'DEF:tx6=$COLLECTDRRD/$HOST/interface/if_octets-tun6to4.rrd:tx:AVERAGE'
        'DEF:tx6_=$COLLECTDRRD/$HOST/interface/if_octets-tun6to4.rrd:tx:MAX'
        'CDEF:mrx4=rx,rx6,-,-1,*'
        'CDEF:mrx4_=rx_,rx6_,-,-1,*'
        'CDEF:tx4=tx,tx6,-'
        'CDEF:tx4_=tx_,tx6_,-'
        'CDEF:mrx6=rx6,-1,*'
        'CDEF:mrx6_=rx6_,-1,*'
        'AREA:tx4_#$ACOLOR4:'
        'AREA:tx6_#$BCOLOR4::STACK'
        'AREA:mrx4_#$ACOLOR2:'
        'AREA:mrx6_#$BCOLOR2::STACK'
        'LINE1:tx4#$LCOLOR4:TX IPv4'
        'LINE1:tx6#$LCOLOR4:TX IPv6:STACK'
        'LINE1:mrx4#$LCOLOR2:RX IPv4'
        'LINE1:mrx6#$LCOLOR2:RX IPv6:STACK'
        'GPRINT:tx:LAST:TX=%5.2lf %sB/s, '        
        'GPRINT:rx:LAST:RX=%5.2lf %sB/s, '        
    """,
#        -u 128000
#        -l -1600000
}

themes = {
    "white": {
        "LCOLOR1": "94541FC0",
        "ACOLOR1": "94541F40",
        "ACOLOR1": "94541F40",
        "LCOLOR2": "948E1FC0",
        "ACOLOR2": "948E1F40",
        "BCOLOR2": "948E1F40",
        "LCOLOR3": "1F9454C0",
        "ACOLOR3": "1F945440",
        "BCOLOR3": "1F945440",
        "LCOLOR4": "1F948EC0",
        "ACOLOR4": "1F948E40",
        "BCOLOR4": "1F948E40",
        "LCOLOR5": "541F94C0",
        "ACOLOR5": "541F9440",
        "BCOLOR5": "541F9440",
        "LCOLOR6": "8E1F94C0",
        "ACOLOR6": "8E1F9440",
        "BCOLOR6": "8E1F9440"
    },
    "black": {
        "BACK":    "000000",
        "SHADEA":  "000000",
        "SHADEB":  "000000",
        "FONT":    "dddddd",
        "CANVAS":  "202020",
        "GRID":    "666666",
        "MGRID":   "aaaaaa",
        "FRAME":   "202020",
        "ARROW":   "FFFFFF",
        "LCOLOR1": "95EC00",
        "BCOLOR1": "80B12C",
        "ACOLOR1": "619A00",
        "LCOLOR2": "028E9B",
        "BCOLOR2": "1E6D74",
        "ACOLOR2": "015C65",
        "LCOLOR3": "FF7800",
        "BCOLOR3": "BFAA30",
        "ACOLOR3": "A68D00",
        "LCOLOR4": "D2006B",
        "BCOLOR4": "9D2763",
        "ACOLOR4": "880045"
    },
    "black2": {
        "BACK":    "000000",
        "SHADEA":  "000000",
        "SHADEB":  "000000",
        "FONT":    "dddddd",
        "CANVAS":  "202020",
        "GRID":    "666666",
        "MGRID":   "aaaaaa",
        "FRAME":   "202020",
        "ARROW":   "FFFFFF",
        "LCOLOR1": "00FF00",
        "ACOLOR1": "408040",
        "LCOLOR2": "0000FF",
        "ACOLOR2": "404080",
        "LCOLOR3": "FFFF00",
        "ACOLOR3": "808040",
        "LCOLOR4": "FF0000",
        "ACOLOR4": "804040"
    }
}


def collectd_flush(timeout=10, path="/var/run/collectd-unixsock"):
    pass
    #sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    #sock.connect(path)
    #sock.send("FLUSH timeout=%d\n" % timeout)
    #sock.recv(2048)
    #sock.close()





if __name__ == "__main__":

    collectd_flush()
    subst = {
        "WIDTH":       "600",
        "HEIGHT":      "150",
        "START":       "-1h",
        "END":         "-0s",
        "COLLECTDRRD": "/var/lib/collectd/rrd/",
    }

    form = cgi.FieldStorage()
    
    def get_arg(name, pattern, default=None):
        if name not in form:
            return default
        value = form.getfirst(name)
        if re.match("^"+pattern+"$", value):
            return value
        else: 
            raise ValueError("argument %s contains illegal characters" % name)

    # Graph type
    graph = get_arg("graph", "[\w-]{1,20}", "load")
    if graph not in graph_templates:
        raise ValueError("Unknown graph type")

    # Host?
    subst["HOST"] = get_arg("host", "[\w-]{1,20}", "atlas")


    # Select theme
    theme = get_arg("theme", "[\w-]{1,20}", "white")
    theme_subs = themes[theme]

    # Start time
    start = get_arg("start", "-\d{1,5}[\w]{1,3}", "-30min")
    subst["START"] = start

    verbose = False
    if "v" in form:
        verbose = True

    # Construct rrd command
    cmd = graph_header
    cmd += graph_templates[graph]
        
    # Theme tags
    for tag in THEME_TAGS:
        if tag in theme_subs:
            cmd += " -c %s#%s\n" % (tag, theme_subs[tag])

    cmd = cmd.replace("\n", "")
    subst.update(theme_subs)

    # Substitute
    cmd_tmpl = Template(cmd)
    cmd = cmd_tmpl.substitute(subst)

    # Generate out
    if verbose:
        print "Content-Type: text/html"     # HTML is following
        print                               # blank line, end of headers
        print cmd
    else:
        print "Content-Type: image/png"     # HTML is following
        print                               # blank line, end of headers
        sys.stdout.flush()
        os.system(cmd)

